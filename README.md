# Exercice API Rest Recherche de parking

### Technologies utilisées

Le projet a été réalisé avec Springboot 2.5.2.

Vous devez avoir Java en version 11 minimum.

Pour les besoin de l'exercice une base mongo en mémoire a été utilisée.

### Installation

``git clone https://gitlab.com/julienbdev/searchparking.git``

``cd searchparking``

``./mvnw clean install``

### Démarrage

``./mvnw spring-boot:run -pl parking-api``

### Test

Pour tester l'application, swagger est disponible : http://localhost:8080/swagger-ui/index.html

Scénario d'utilisation :

Il faut d'abord obtenir la liste des villes disponibles :

http://localhost:8080/cities

Exemple de réponse :
```json
[
    {
        "zipCode": "33000",
        "name": "Bordeaux"
    }
]
```

Ensuite à partir du zipCode de la ville choisie, on peut faire une recherche en précisant les coordonnées de la position et une distance maximum en mètre à partir de la position.

http://localhost:8080/cities/33000/parkings?latitude=44.888824&longitude=-0.517676&distance=1000

```json
[
    {
        "id": "ST_PARK_P.211",
        "name": "Parc-Relais La Gardette ",
        "address": "Chemin du Grand Came 33310 Lormont",
        "latitude": 44.888824,
        "longitude": -0.517676
    },
    {
        "id": "ST_PARK_P.263",
        "name": "Parc-Relais La Gardette ",
        "address": "Chemin du Grand Came 33530 Bassens",
        "latitude": 44.887807,
        "longitude": -0.518228
    }
]
```

Il est possible d'avoir plus d'info sur un parking à partir de son identifiant.

http://localhost:8080/cities/33000/parkings/ST_PARK_P.211
```json
{
    "id": "ST_PARK_P.211",
    "name": "Parc-Relais La Gardette ",
    "address": "Chemin du Grand Came 33310 Lormont",
    "latitude": 44.888824,
    "longitude": -0.517676,
    "nbFreePlaces": 155,
    "nbTotalplaces": 242
}
```

### Spring service locator

Chaque ville a sa propre implémentation pour obtenir la liste des parkings, effectuer la recherche ou obtenir les infos détaillées.
Pour cela j'ai utilisé Spring Service Locator.

L'interface ParkingService doit être implémentée pour chaque ville, et il faut lui associer le nom du bean pour le Service Locator.

Exemple pour la ville de Bordeaux :
```json
{
  "_class": "org.julienbdev.parking.entity.City",
  "zipCode": "33000",
  "name": "Bordeaux",
  "locatorBeanName": "bordeauxDriver"
}
```
Le bean Spring "bordeauxDriver" devra implémenter l'interface ParkingService.

### Recherche geoloc

Pour la recherche geolocalisée, j'ai utilisé une base de donné MongoDB avec les opérations géospatial de mongo.
Les données récupérées via l'URL sont convertis au format GeoJSON.

Ce format utilise la même norme EPSG:4326 que sur http://data.lacub.fr, ce qui permet une conversion facile.

### TTL

Pour limiter le nombre de requêtes effectuées vers http://data.lacub.fr, j'ai utilisé un système de cache simplifié basé sur le TTL mongo.

Si le TTL de mon objet Cache n'est pas expiré, je fais directement une recherche géoloc dans la base mongo.
Sinon je met d'abord à jour la base en apellant l'url http://data.lacub.fr.

J'ai fais ce choix car le nombre de parking n'évolue pas souvent. 
De plus il est possible que selon les partenaires externes, l'appel à une API externe soit payant, d'où l'interêt de limiter les appels.
Pour l'exercice le delai d'expiration a été fixé à 5min.

### Amélioration possible

Il faudrait sécuriser l'accès à l'API, avec par exemple une clé passée en paramètre pour identifier l'utilisateur de l'API.
