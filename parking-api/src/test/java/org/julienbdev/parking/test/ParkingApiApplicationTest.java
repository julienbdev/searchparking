package org.julienbdev.parking.test;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.julienbdev.parking.bordeaux.dto.FeatureCollection;
import org.julienbdev.parking.bordeaux.service.impl.BordeauxParkingService;
import org.julienbdev.parking.dto.Parking;
import org.julienbdev.parking.dto.ParkingDetails;
import org.julienbdev.parking.entity.City;
import org.julienbdev.parking.service.factory.ParkingServiceFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
public class ParkingApiApplicationTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
    @Qualifier("wfsRestTemplate")
    private RestTemplate restTemplate;
    
    @InjectMocks
    @Qualifier("bordeauxDriver")
    private BordeauxParkingService parkingService;
    
    @Mock
    private ParkingServiceFactory parkingServiceFactory;

	@Autowired
	private ObjectMapper objectMapper;
	
	@BeforeEach
	private void setupMock() {
		File file = new File("src/test/resources/wfs.xml");
		JAXBContext jaxbContext;
		
		try {
			jaxbContext = JAXBContext.newInstance(FeatureCollection.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			
			FeatureCollection featureCollection = (FeatureCollection) jaxbUnmarshaller.unmarshal(file);
			when(restTemplate.getForObject(Mockito.anyString(), Mockito.any())).thenReturn(featureCollection);
		} catch (JAXBException e) {
			Assertions.fail("Exception should not have been thrown.", e);
		}
		
		// mock bordeauxDriver to inject our test parking datas
		when(parkingServiceFactory.getParkingServiceDriver("bordeauxDriver")).thenReturn(parkingService);
	}

	@Test
	public void testParkingApiBordeauxDriver() throws Exception {
		// Test /cities
		MvcResult resultCities = mockMvc.perform(MockMvcRequestBuilders.get("/cities"))	
				.andExpect((status().isOk()))
				.andReturn();

		List<City> cities = objectMapper.readValue(resultCities.getResponse().getContentAsString(), new TypeReference<List<City>>() {});
		Assertions.assertNotNull(cities);
		Assertions.assertTrue(cities.size() > 0);

		City city = cities.get(0);
		Assertions.assertNotNull(city.getZipCode());

		// Test /cities/{zipCode}/parkings
		String zipCode = city.getZipCode();
		StringBuilder url = new StringBuilder();
		url.append("/cities/");
		url.append(zipCode);
		url.append("/parkings?latitude=44.888824&longitude=-0.517676&distance=1000");

		MvcResult resultParkings = mockMvc.perform(MockMvcRequestBuilders.get(url.toString()))	
				.andExpect((status().isOk()))
				.andReturn();

		List<Parking> parkings = objectMapper.readValue(resultParkings.getResponse().getContentAsString(), new TypeReference<List<Parking>>() {});
		Assertions.assertNotNull(parkings);
		Assertions.assertTrue(parkings.size() == 2);
		
		Parking parking = parkings.get(0);
		Assertions.assertEquals(parking.getId(), "ST_PARK_P.211");
		Assertions.assertEquals(parking.getName(), "Parc-Relais La Gardette TEST");
		
		// Test /cities/{zipCode}/parkings/{parkingId}
		StringBuilder urlParkingDetails = new StringBuilder();
		urlParkingDetails.append("/cities/");
		urlParkingDetails.append(zipCode);
		urlParkingDetails.append("/parkings/");
		urlParkingDetails.append(parking.getId());
		
		MvcResult resultParkingDetails = mockMvc.perform(MockMvcRequestBuilders.get(urlParkingDetails.toString()))	
				.andExpect((status().isOk()))
				.andReturn();
		
		ParkingDetails parkingDetails = objectMapper.readValue(resultParkingDetails.getResponse().getContentAsString(), ParkingDetails.class);
		Assertions.assertNotNull(parkingDetails);
		Assertions.assertEquals(parkingDetails.getId(), "ST_PARK_P.211");
		Assertions.assertEquals(parkingDetails.getName(), "Parc-Relais La Gardette TEST");
		Assertions.assertEquals(parkingDetails.getNbFreePlaces(), 217);
		Assertions.assertEquals(parkingDetails.getNbTotalplaces(), 242);
	}
}
