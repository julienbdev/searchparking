package org.julienbdev.parking.api.controller;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.julienbdev.parking.dto.Parking;
import org.julienbdev.parking.dto.ParkingDetails;
import org.julienbdev.parking.entity.City;
import org.julienbdev.parking.service.CityService;
import org.julienbdev.parking.service.ParkingService;
import org.julienbdev.parking.service.factory.ParkingServiceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("cities/{zipCode}/parkings")
public class ParkingController {
	
	@Autowired
	private ParkingServiceFactory parkingServiceFactory;
	
	@Autowired
	private CityService cityService;

	@GetMapping
	@ApiOperation("Search parking lot near a location")
	public List<Parking> getParkings(
			@ApiParam(value = "city zipcode", example = "33000")
			@PathVariable("zipCode") String zipCode,
			@ApiParam(value = "Latitude coordinate", example = "44.888824")
			@RequestParam BigDecimal latitude,
			@ApiParam(value = "Longitude coordinate", example = "-0.517676")
			@RequestParam BigDecimal longitude,
			@ApiParam(value = "Maximum distance in meters ", example = "1000")
			@RequestParam Integer distance ) {
		
		ParkingService parkingService = getParkingServiceDriver(zipCode);
		
		return parkingService.search(latitude, longitude, distance);
	}
	
	@GetMapping("{parkingId}")
	@ApiOperation("Get detailed informations about a parking lot")
	public ParkingDetails getParkingDetails(
			@ApiParam(value = "City zipcode", example = "33000")
			@PathVariable("zipCode") String zipCode,
			@ApiParam(value = "Parking id", example = "ST_PARK_P.211")
			@PathVariable("parkingId") String parkingId) {
		
		ParkingService parkingService = getParkingServiceDriver(zipCode);
			
		return parkingService.getParkingDetails(parkingId);
	}
	
	private ParkingService getParkingServiceDriver(String zipCode) {
		Optional<City> cityOpt = cityService.getCityByZipCode(zipCode);
		
		// If city is not found, NoSuchElementException is thrown by Optional.get()
		City city = cityOpt.get();
		
		// If city specific driver is not found, NoSuchBeanDefinitionException is thrown
		return parkingServiceFactory.getParkingServiceDriver(city.getLocatorBeanName());
		
	}
}
