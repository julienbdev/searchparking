package org.julienbdev.parking.api.controller;

import java.util.List;

import org.julienbdev.parking.entity.City;
import org.julienbdev.parking.service.CityService;
import org.julienbdev.parking.view.Views;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("cities")
public class CityController {

	@Autowired
	private CityService cityService;
	
	@GetMapping
	@JsonView(Views.Public.class)
	@ApiOperation("Get all cities managed by API")
	public List<City> getCities() {
		return cityService.getAvailableCities();
	}
}
