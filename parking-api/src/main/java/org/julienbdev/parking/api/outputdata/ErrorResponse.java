package org.julienbdev.parking.api.outputdata;

import lombok.Data;

/**
 * 
 * Simple class to encapsulate error code and message return by API
 *
 */
@Data
public class ErrorResponse {
	
	private String code;

	private String message;
	
	public ErrorResponse() {

	}

	public ErrorResponse(String code, String message) {
		this.code = code;
		this.message = message;
	}
	
}
