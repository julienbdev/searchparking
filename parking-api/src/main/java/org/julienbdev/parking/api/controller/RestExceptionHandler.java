package org.julienbdev.parking.api.controller;

import java.util.NoSuchElementException;

import org.julienbdev.parking.api.outputdata.ErrorResponse;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler({ NoSuchElementException.class })
	public ResponseEntity<Object> handleNoSuchElementException(final NoSuchElementException ex, final WebRequest request) {
		this.logger.error(ex.getMessage(), ex);
		ErrorResponse error = new ErrorResponse(HttpStatus.NOT_FOUND.name(), ex.getMessage());
		return this.handleExceptionInternal(ex, error, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
	}

	@ExceptionHandler({ NoSuchBeanDefinitionException.class })
	public ResponseEntity<Object> handleNoSuchBeanDefinitionException(final NoSuchBeanDefinitionException ex, final WebRequest request) {
		this.logger.error(ex.getMessage(), ex);
		ErrorResponse error = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.name(), ex.getMessage());
		return this.handleExceptionInternal(ex, error, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
	}
	
	@ExceptionHandler({ Exception.class })
	public ResponseEntity<Object> handleInternal(final Exception ex, final WebRequest request) {
		this.logger.error("500 Status Code", ex);
		final String errorMessage = "An error occured. Please retry later.";
		ErrorResponse error = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.name(), errorMessage);
		return this.handleExceptionInternal(ex, error, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
	}
}
