package org.julienbdev.parking.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SpringFoxConfig {
	
    @Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2) 
          .select()                                  
          .apis(RequestHandlerSelectors.basePackage("org.julienbdev.parking"))              
          .paths(PathSelectors.any())                          
          .build()
          .apiInfo(metadata());
    }
    
    private ApiInfo metadata() {
        return new ApiInfoBuilder()
                .title("Search parking lot REST API")
                .description("Spring Boot REST API for parking lot search near a location")
                .build();
    }
}
