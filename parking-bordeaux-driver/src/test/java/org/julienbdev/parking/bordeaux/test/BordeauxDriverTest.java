package org.julienbdev.parking.bordeaux.test;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.julienbdev.parking.bordeaux.dto.FeatureCollection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BordeauxDriverTest {

	@Test
	public void testUnmarshall() {
		
		File file = new File("src/test/resources/wfs.xml");
		JAXBContext jaxbContext;
		
		try {
			jaxbContext = JAXBContext.newInstance(FeatureCollection.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			
			FeatureCollection featureCollection = (FeatureCollection) jaxbUnmarshaller.unmarshal(file);
			
			Assertions.assertNotNull(featureCollection);
			Assertions.assertNotNull(featureCollection.getFeatureMembers());
			Assertions.assertTrue(featureCollection.getFeatureMembers().size() == 86);
			
		} catch (JAXBException e) {
			Assertions.fail("Exception should not have been thrown.", e);
		}
		
	}
}
