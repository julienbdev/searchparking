package org.julienbdev.parking.bordeaux.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@XmlRootElement(name = "Point", namespace = "http://www.opengis.net/gml")
@XmlAccessorType(XmlAccessType.FIELD) 
public class Point {
	
	@XmlElement(name = "pos", namespace="http://www.opengis.net/gml")
	private String pos;
	
}
