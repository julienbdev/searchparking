package org.julienbdev.parking.bordeaux.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;

import lombok.Data;

@Data
@XmlRootElement(name = "FeatureCollection", namespace = "http://www.opengis.net/wfs")
@XmlAccessorType(XmlAccessType.FIELD) 
public class FeatureCollection {

	@XmlElement(name="boundedBy", namespace = "http://www.opengis.net/gml")
	private BoundedBy boundedBy;
	
	@XmlElement(name="featureMember", namespace = "http://www.opengis.net/gml")
	private List<FeatureMember> featureMembers;
	
}