package org.julienbdev.parking.bordeaux.repository;

import java.util.List;

import org.julienbdev.parking.bordeaux.entity.Place;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PlaceRepository extends MongoRepository<Place, String> {

	List<Place> findByLocationNear(Point p, Distance d);
}
