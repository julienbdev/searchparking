package org.julienbdev.parking.bordeaux.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@XmlRootElement(name = "ST_PARK_P", namespace = "http://data.bordeaux-metropole.fr/wfs")
@XmlAccessorType(XmlAccessType.FIELD) 
public class Park {

	@XmlAttribute(name="id", namespace = "http://www.opengis.net/gml")
	private String id;
	
	@XmlElement(name="boundedBy", namespace = "http://www.opengis.net/gml")
	private BoundedBy boundedBy;
	
	@XmlElement(name="geometry", namespace = "http://data.bordeaux-metropole.fr/wfs")
	private Geometry geometry;
	
	@XmlElement(name="NOM", namespace = "http://data.bordeaux-metropole.fr/wfs")
	private String name;
	
	@XmlElement(name="LIBRES", namespace = "http://data.bordeaux-metropole.fr/wfs")
	private Integer nbFreePlaces;
	
	@XmlElement(name="TOTAL", namespace = "http://data.bordeaux-metropole.fr/wfs")
	private Integer nbTotalplaces;
	
	@XmlElement(name="ADRESSE", namespace = "http://data.bordeaux-metropole.fr/wfs")
	private String address;
}