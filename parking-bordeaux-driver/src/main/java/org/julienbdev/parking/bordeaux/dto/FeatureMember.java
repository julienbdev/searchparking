package org.julienbdev.parking.bordeaux.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@XmlRootElement(name = "featureMember", namespace = "http://www.opengis.net/gml")
@XmlAccessorType(XmlAccessType.FIELD) 
public class FeatureMember {

	@XmlElement(name = "ST_PARK_P", namespace = "http://data.bordeaux-metropole.fr/wfs")
	private Park park;
	
}