package org.julienbdev.parking.bordeaux.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@XmlRootElement(name = "geometry", namespace = "http://data.bordeaux-metropole.fr/wfs")
@XmlAccessorType(XmlAccessType.FIELD) 
public class Geometry {

	@XmlElement(name="Point", namespace = "http://www.opengis.net/gml")
	private Point point;
	
}
