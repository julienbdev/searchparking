package org.julienbdev.parking.bordeaux.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.julienbdev.parking.bordeaux.dto.FeatureCollection;
import org.julienbdev.parking.bordeaux.dto.FeatureMember;
import org.julienbdev.parking.bordeaux.dto.Park;
import org.julienbdev.parking.bordeaux.entity.Place;
import org.julienbdev.parking.bordeaux.repository.PlaceRepository;
import org.julienbdev.parking.dto.Parking;
import org.julienbdev.parking.dto.ParkingDetails;
import org.julienbdev.parking.repository.CacheRepository;
import org.julienbdev.parking.service.CacheService;
import org.julienbdev.parking.service.ParkingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.geo.GeoJsonPoint;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service("bordeauxDriver")
public class BordeauxParkingService implements ParkingService {
	
	// For the exercise the url is hardcoded
	private static final String resourceUrl = "http://data.lacub.fr/wfs?key=9Y2RU3FTE8&SERVICE=WFS&VERSION=1.1.0&REQUEST=GetFeature&TYPENAME=ST_PARK_P&SRSNAME=EPSG:4326";
	
	private static final String ZIP_CODE = "33000";
	
	private Logger logger = LoggerFactory.getLogger(BordeauxParkingService.class);
	
	@Autowired
	@Qualifier("wfsRestTemplate")
	private RestTemplate restTemplate;
	
	@Autowired
	private PlaceRepository placeRepository;
	
	@Autowired
	private CacheService cacheService;
	
	@Override
	public List<Parking> search(BigDecimal latitude, BigDecimal longitude, Integer distance) {
		// get all datas and store them into our geoloc database
		createGeoLocDatas();		
		
		Point point = new Point(longitude.doubleValue(), latitude.doubleValue());
		Distance distanceMeters = new Distance(distance / 1000, Metrics.KILOMETERS);
		
		List<Place> places = placeRepository.findByLocationNear(point, distanceMeters);
		
		List<Parking> parkings = places.stream().map(place -> buildParking(place)).collect(Collectors.toList());
		
		return parkings;
	}

	@Override
	public ParkingDetails getParkingDetails(String parkingId) {
		// get all datas and store them into our geoloc database
		createGeoLocDatas();
		
		Optional<Place> place = placeRepository.findById(parkingId);
		return buildParkingDetails(place.get());
		
	}
	
	/**
	 * Get parking list from external provider
	 * 
	 * @return FeatureCollection, or null if an error occured
	 */
	private FeatureCollection getDatas() {
		FeatureCollection response; 
		
		try {
			response = restTemplate.getForObject(resourceUrl, FeatureCollection.class);
		} catch(RestClientException e) {
			logger.error("Can't get parking list", e);
			return null;
		}
		
		return response;
	}
	
	/**
	 * Get coordinates from a FeatureMember object.
	 * Coordinates format : [latitude, longitude]
	 * 
	 * @param featureMember
	 * @return coordinates if valid, else null
	 */
	private String[] getCoordinates(FeatureMember featureMember) {
		if(featureMember.getPark() != null) {
			Park park = featureMember.getPark();
			if(park.getGeometry() != null && park.getGeometry().getPoint() != null) {
				String position = park.getGeometry().getPoint().getPos();
				if(StringUtils.hasLength(position)) {
					String[] coordinates = position.split(" ");
					if(coordinates.length == 2) {
						return coordinates;
					}
				}
			}
		}
		return null;
	}
	
	/**
	 * If cache is expired, we refresh all parkings informations.
	 * A request is made to the external provider.
	 * We replace old parking list by new one.
	 * 
	 */
	private void createGeoLocDatas() {
		if(!cacheService.isCached(ZIP_CODE)) {
			// If cache is expired, we recreate geoloc datas
			logger.debug("Cache is expired for zipCode : " + ZIP_CODE);
			FeatureCollection featureCollection = getDatas();
			if(featureCollection != null && !CollectionUtils.isEmpty(featureCollection.getFeatureMembers())) {
				List<Place> places = new ArrayList<>();
				
				featureCollection.getFeatureMembers().forEach(featureMember -> {
					Place place = buildPlace(featureMember);
					
					if(place != null) {
						places.add(place);	
					}
				});
				
				placeRepository.deleteAll();
				placeRepository.saveAll(places);
				
				logger.debug("cache created for zipCode : " + ZIP_CODE);
				cacheService.createCacheInfo(ZIP_CODE);
			}			
		}
	}
	
	private Place buildPlace(FeatureMember featureMember) {
		String[] coordinates = getCoordinates(featureMember);
		
		if(coordinates != null) {
			Park park = featureMember.getPark();
			Place place = new Place();
			place.setId(park.getId());
			place.setAddress(park.getAddress());
			place.setName(park.getName());
			place.setNbFreePlaces(park.getNbFreePlaces());
			place.setNbTotalplaces(park.getNbTotalplaces());
			
			GeoJsonPoint location = new GeoJsonPoint(Double.valueOf(coordinates[1]), Double.valueOf(coordinates[0]));
			place.setLocation(location);
			
			return place;
		}
		
		return null;
	}
	
	private Parking buildParking(Place place) {
		Parking parking = new Parking();
		
		parking.setId(place.getId());
		parking.setName(place.getName());
		parking.setAddress(place.getAddress());
		parking.setLatitude(BigDecimal.valueOf(place.getLocation().getY()));
		parking.setLongitude(BigDecimal.valueOf(place.getLocation().getX()));
		
		return parking;
	}
	
	private ParkingDetails buildParkingDetails(Place place) {
		ParkingDetails parkingDetails = new ParkingDetails();
		
		parkingDetails.setId(place.getId());
		parkingDetails.setName(place.getName());
		parkingDetails.setAddress(place.getAddress());
		parkingDetails.setLatitude(BigDecimal.valueOf(place.getLocation().getY()));
		parkingDetails.setLongitude(BigDecimal.valueOf(place.getLocation().getX()));
		parkingDetails.setNbFreePlaces(place.getNbFreePlaces());
		parkingDetails.setNbTotalplaces(place.getNbTotalplaces());
		
		return parkingDetails;
	}
}
