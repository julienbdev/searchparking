package org.julienbdev.parking.bordeaux.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestConfig {

	@Bean
	public RestTemplate wfsRestTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		// WFS media type is not a regular media type recognized by Spring
		// we assume it is text/XML
		restTemplate.getInterceptors().add((request, body, execution) -> {
            ClientHttpResponse response = execution.execute(request,body);
            response.getHeaders().setContentType(MediaType.TEXT_XML);
            return response;
        });
		
		return restTemplate;
	}
	
}
