package org.julienbdev.parking.entity;

import org.julienbdev.parking.view.Views;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonView;

import lombok.Data;

@Data
@Document
@JsonView(Views.Public.class)
public class City {
	
	// Internal view, no need to expose this field in API
	@JsonView(Views.Internal.class)
	@Id
	private String id;
	
	private String zipCode;
	
	private String name;
	
	// Internal view, no need to expose this field in API
	@JsonView(Views.Internal.class)
	private String locatorBeanName; 
}
