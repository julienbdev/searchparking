package org.julienbdev.parking.entity;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document
public class Cache {

	@Id
	protected String id;
	
	protected String zipCode;
	
    @Indexed(expireAfterSeconds=300)
    protected LocalDateTime registeredDate;
	
}
