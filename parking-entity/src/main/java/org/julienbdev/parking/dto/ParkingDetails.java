package org.julienbdev.parking.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ParkingDetails extends Parking {
	
	private Integer nbFreePlaces;
	
	private Integer nbTotalplaces;
	
}
