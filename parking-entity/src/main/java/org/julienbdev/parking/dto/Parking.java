package org.julienbdev.parking.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class Parking {

	protected String id;
	
	protected String name;
	
	protected String address;
	
	protected BigDecimal latitude;
	
	protected BigDecimal longitude;
}
