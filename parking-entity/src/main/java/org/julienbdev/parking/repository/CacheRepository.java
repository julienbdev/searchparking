package org.julienbdev.parking.repository;

import java.util.Optional;

import org.julienbdev.parking.entity.Cache;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CacheRepository extends MongoRepository<Cache, String> {

	Optional<Cache> findByZipCode(String zipCode);
	
}
