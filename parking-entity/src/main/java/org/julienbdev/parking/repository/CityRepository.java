package org.julienbdev.parking.repository;

import java.util.Optional;

import org.julienbdev.parking.entity.City;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends MongoRepository<City, String> {

	 Optional<City> findById(String cityId);
	 
	 Optional<City> findByZipCode(String zipCode);
}
