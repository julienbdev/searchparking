package org.julienbdev.parking.service;

public interface CacheService {

	Boolean isCached(String zipCode);
	
	void createCacheInfo(String zipCode);
	
}
