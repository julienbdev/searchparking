package org.julienbdev.parking.service.factory.config;

import org.julienbdev.parking.service.factory.ParkingServiceFactory;
import org.springframework.beans.factory.config.ServiceLocatorFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ParkingServiceLocatorConfig {

	  @Bean("parkingServiceFactory")
	  public ServiceLocatorFactoryBean serviceLocatorFactoryBean() {
	    ServiceLocatorFactoryBean factoryBean = new ServiceLocatorFactoryBean();
	    factoryBean.setServiceLocatorInterface(ParkingServiceFactory.class);
	    return factoryBean;
	  }
}
