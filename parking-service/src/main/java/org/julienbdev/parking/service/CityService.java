package org.julienbdev.parking.service;

import java.util.List;
import java.util.Optional;

import org.julienbdev.parking.entity.City;

public interface CityService {

	List<City> getAvailableCities();
	
	Optional<City> getCityById(String cityId);
	
	Optional<City> getCityByZipCode(String zipCode);
}
