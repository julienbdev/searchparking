package org.julienbdev.parking.service.factory;

import org.julienbdev.parking.service.ParkingService;

public interface ParkingServiceFactory {

	ParkingService getParkingServiceDriver(String locatorBeanName);
}
