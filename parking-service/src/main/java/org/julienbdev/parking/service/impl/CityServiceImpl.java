package org.julienbdev.parking.service.impl;

import java.util.List;
import java.util.Optional;

import org.julienbdev.parking.entity.City;
import org.julienbdev.parking.repository.CityRepository;
import org.julienbdev.parking.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CityServiceImpl implements CityService {

	@Autowired
	private CityRepository cityRepository;
	
	@Override
	public List<City> getAvailableCities() {
		return cityRepository.findAll();
	}

	@Override
	public Optional<City> getCityById(String cityId) {
		return cityRepository.findById(cityId);
	}

	@Override
	public Optional<City> getCityByZipCode(String zipCode) {
		return cityRepository.findByZipCode(zipCode);
	}

}
