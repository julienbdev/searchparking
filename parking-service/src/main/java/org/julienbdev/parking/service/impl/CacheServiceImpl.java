package org.julienbdev.parking.service.impl;

import java.time.LocalDateTime;
import java.util.Optional;

import org.julienbdev.parking.entity.Cache;
import org.julienbdev.parking.repository.CacheRepository;
import org.julienbdev.parking.service.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CacheServiceImpl implements CacheService {

	@Autowired
	private CacheRepository cacheRepository;
	
	@Override
	public Boolean isCached(String zipCode) {
		Optional<Cache> cacheOpt = cacheRepository.findByZipCode(zipCode);
		return cacheOpt.isPresent();
	}

	@Override
	public void createCacheInfo(String zipCode) {
		Cache cache = new Cache();
		cache.setZipCode(zipCode);
		cache.setRegisteredDate(LocalDateTime.now());
		
		cacheRepository.save(cache);
	}

}
