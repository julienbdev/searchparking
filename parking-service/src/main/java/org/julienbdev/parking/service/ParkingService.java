package org.julienbdev.parking.service;

import java.math.BigDecimal;
import java.util.List;

import org.julienbdev.parking.dto.Parking;
import org.julienbdev.parking.dto.ParkingDetails;

public interface ParkingService {

	/**
	 * Method to implement for parking search from a position defined by latitude and longitude.
	 * Search perimeter is defined by the distance from the position.
	 * 
	 * @param latitude in decimal degrees
	 * @param longitude in decimal degrees
	 * @param distance in meters unit
	 * @return Parkings matching search parameters, or empty if nothing found.
	 */
	List<Parking> search(BigDecimal latitude, BigDecimal longitude, Integer distance);
	
	/**
	 * Method to implement for parking details.
	 * 
	 * @param parkingId obtained from a previous search
	 * @return ParkingDetails if found
	 */
	ParkingDetails getParkingDetails(String parkingId);
}
